import Vue from 'vue'
import App from './App.vue'
import store from './store'
import httpRequest from './utils/httpRequest'

import ScheduleCalendar from "../packages";

Vue.config.productionTip = false
Vue.use(ScheduleCalendar)

// import Vconsole from 'vconsole'
// const vConsole = new Vconsole()
// export default vConsole

Vue.prototype.$http = httpRequest

new Vue({
  store: store({
    modules: {
      appState: ScheduleCalendar.appState
    }
  }),
  render: h => h(App)
}).$mount('#app')
