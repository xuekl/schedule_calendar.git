import Schedule from './schedule'
import appState from './store/modules/appState'
import VueScroller from 'vue-scroller'

const components = [
  // Schedule.OneDayCalendar,
  Schedule.OneDayCalendarSmall,
  // Schedule.ThreeDaysCalendar,
  Schedule.ThreeDaysCalendarSmall,
  // Schedule.WeekCalendar,
  Schedule.WeekCalendarSmall,
  // Schedule.ListCalendar,
  Schedule.ListCalendarSingle,
  Schedule.MonthCalendar
]

const install = function (Vue) {
  if (install.installed) return
  Vue.use(VueScroller)
  components.map(component => Vue.component(component.name, component))
}


if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}


export default {
  install,
  Schedule,
  appState
}
