// import OneDayCalendar from './one-day-calendar/Calendar'
import OneDayCalendarSmall from './one-day-calendar-small/Calendar'
// import ThreeDaysCalendar from './three-days-calendar/Calendar'
import ThreeDaysCalendarSmall from './three-days-calendar-small/Calendar'
// import WeekCalendar from './week-calender/Calendar'
import WeekCalendarSmall from './week-calender-small/Calendar'
// import ListCalendar from './list-calendar/Calendar'
import ListCalendarSingle from './list-calender-single/Calendar'
import MonthCalendar from './month-calendar/Calendar'

// const els = [OneDayCalendar, ThreeDaysCalendar, ListCalendar]
// els.forEach(el => {
//   el.install = function (Vue) {
//     Vue.component(el.name, el);
//   };
// })

// OneDayCalendar.install = function (Vue) {
//   Vue.component(OneDayCalendar.name, OneDayCalendar);
// };

OneDayCalendarSmall.install = function (Vue) {
  Vue.component(OneDayCalendarSmall.name, OneDayCalendarSmall);
};

// ThreeDaysCalendar.install = function (Vue) {
//   Vue.component(ThreeDaysCalendar.name, ThreeDaysCalendar);
// };

ThreeDaysCalendarSmall.install = function (Vue) {
  Vue.component(ThreeDaysCalendarSmall.name, ThreeDaysCalendarSmall);
};

// WeekCalendar.install = function (Vue) {
//   Vue.component(WeekCalendar.name, WeekCalendar);
// };

WeekCalendarSmall.install = function (Vue) {
  Vue.component(WeekCalendarSmall.name, WeekCalendarSmall);
};


// ListCalendar.install = function (Vue) {
//   Vue.component(ListCalendar.name, ListCalendar);
// };

ListCalendarSingle.install = function (Vue) {
  Vue.component(ListCalendarSingle.name, ListCalendarSingle);
};

MonthCalendar.install = function (Vue) {
  Vue.component(MonthCalendar.name, MonthCalendar)
}


export default {
  // OneDayCalendar,
  OneDayCalendarSmall,
  // ThreeDaysCalendar,
  ThreeDaysCalendarSmall,
  // WeekCalendar,
  WeekCalendarSmall,
  // ListCalendar,
  ListCalendarSingle,
  MonthCalendar
};
