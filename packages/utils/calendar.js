import moment from "moment";

const week = ['一', '二', '三', '四', '五', '六', '日'];

// 计算是周几
export function calcWeekDay(year, month, day) {
  const index = moment(year, month, day).day() - 1;
  return week[index];
}

export function weekDayToStr (index) {
  return week[index - 1];
}

// 计算是否是闰年
function isLeap (year) {
  return  (year % 100 != 0 && year % 4 == 0) || year % 400 == 0;
}

// 计算当月天数
export function calcDays ({year, month}) {
  let num = 31;
  switch(month) {
    case 2:
      num = isLeap(year) ? 29 : 28;
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      num = 30;
      break;
  }
  return num;
}

// 计算这个月的1号是周几
export function calcWeekOfFirstDay (year, month) {
  return moment(year + '-'+ month.toString().padStart(2, '0') + '-' + '01').day();
}

// 获取当前日期
export function getCurrentDay () {
  return moment(Date().now()).format('YYYY-MM-DD');
}

// 获取当前时间
export function getCurrentTime () {
  return moment(Date().now()).format('YYYY-MM-DD HH:nn');
}
